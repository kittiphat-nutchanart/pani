const Hapi = require('@hapi/hapi');

const { Model } = require('objection');
const Knex = require('knex');

const knexConfigs = require('../knexfile.js');

// Give the knex instance to objection.
const knex = Knex(knexConfigs.default);
Model.knex(knex);


const server = Hapi.server({
    port: 3000,
    host: 'localhost',
    debug: { request: ['error'] },
});


module.exports = (async () => {

    // add more routes here
    server.route(require('../routes/createTicket/route.js'));
    server.route(require('../routes/updateTicket/route.js'));
    server.route(require('../routes/listTickets/route.js'));

    return server;
})();


process.on('unhandledRejection', (err) => {

    console.error(err);
    process.exit(1);
});
