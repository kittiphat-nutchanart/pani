const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');

const knexConfigs = require('../../knexfile.js');
const {
    Ticket,
} = require('../index.js');


async function main() {
    // set up database
    const knex = Knex(knexConfigs.testing);
    objection.Model.knex(knex);

    tap.test('models can be inserted', async (t) => {

        // knex setup and teardown
        await knex.initialize();
        await knex.migrate.latest();
        t.teardown(async () => {
            await knex.destroy();
        });

        await Ticket.query().insert({
            title: 'ticket',
            description: 'desc',
            contact: 'contact'
        });
    });

    tap.test('updated_at gets updated', async (t) => {

        // given
        // knex setup and teardown
        await knex.initialize();
        await knex.migrate.latest();
        t.teardown(async () => {
            await knex.destroy();
        });


        // when
        const ticketBefore = await Ticket.query().insertAndFetch({
            title: 'ticket',
            description: 'desc',
            contact: 'contact'
        });

        const ticketAfter = await ticketBefore.$query().patchAndFetch({
            title: 'new title'
        });


        // then
        t.notEqual(ticketAfter, ticketBefore);
    });
}

main();
