const { Model } = require('objection');

class Ticket extends Model {
    static get tableName() {
        return 'tickets';
    }

    $beforeUpdate() {
        this.updated_at = new Date().toISOString();
    }
}

module.exports = {
    Ticket
};
