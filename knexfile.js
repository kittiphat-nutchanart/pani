const path = require('path');

const migrations = {
    tableName: 'knex_migrations',
    directory: path.join(__dirname, 'migrations')
};

module.exports = {
    testing: {
        client: 'sqlite3',
        useNullAsDefault: true,
        connection: ':memory:',
        migrations,
        // debug: true
    },
    default: {
        client: 'postgresql',
        connection: {
            host: process.env.PGHOST,
            user: process.env.PGUSER,
            password: process.env.PGPASSWORD,
            database: process.env.PGDATABASE,
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations
    }
};
