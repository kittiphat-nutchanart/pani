require('dotenv').config();

const serverPromise = require('./server');

async function main() {
    const server = await serverPromise;
    await server.start();
    console.log('Server running on %s', server.info.uri);
}

main();
