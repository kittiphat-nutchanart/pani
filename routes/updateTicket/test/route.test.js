const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');

const schema = require('../schema.js');
const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const {
    Ticket
} = require('../../../models');


async function main() {
    // init knex and tell objection.js to use it
    const knex = Knex(knexConfigs.testing);
    objection.Model.knex(knex);
    const server = await serverPromise;

    tap.test('a ticket\'s status can be updated', async (t) => {
        // given
        // knex setup
        await knex.initialize();
        await knex.migrate.latest();
        t.teardown(async () => {
            await knex.destroy();
        });

        // when
        const ticket = await Ticket.query()
              .insertAndFetch({
                  title: 'my title',
                  description: 'my description',
                  contact: 'my contact'
              });
        const newStatus = 'accepted';
        const res = await server.inject({
            method: 'PATCH',
            url: `/api/ticket/${ticket.id}`,
            payload: {
                status: newStatus,
            }
        });

        // then
        t.equal(res.statusCode, 200);
        Joi.assert(res.result, schema);
        t.equal(res.result.status, newStatus);
    });
};


main();
