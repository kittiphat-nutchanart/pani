const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');
const assert = require('assert').strict;

const { Ticket } = require('../../models');

module.exports = {
    method: 'PATCH',
    path: '/api/ticket/{id}',
    options: {
        description: 'this endpoint updates a ticket\'s status',
        validate: {
            payload: Joi.object({
                status: Joi.string()
            }).prefs({ presence: 'required' })
        }
    },
    handler: async (request, h) => {
        const ticket = await Ticket.query()
              .patchAndFetchById(request.params.id, request.payload);

        return ticket;
    }
};
