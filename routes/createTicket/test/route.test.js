const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');

const schema = require('../schema.js');
const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const {
    Ticket
} = require('../../../models');


async function main() {
    // init knex and tell objection.js to use it
    const knex = Knex(knexConfigs.testing);
    objection.Model.knex(knex);
    const server = await serverPromise;

    tap.test('a ticket can be created', async (t) => {
        // given
        // knex setup
        await knex.initialize();
        await knex.migrate.latest();
        t.teardown(async () => {
            await knex.destroy();
        });

        // when
        const expectedTitle = 'my title';
        const expectedDescription = 'my description';
        const expectedContact = 'my contact';
        const res = await server.inject({
            method: 'POST',
            url: '/api/ticket',
            payload: {
                title: expectedTitle,
                description: expectedDescription,
                contact: expectedContact
            }
        });

        // then
        t.equal(res.statusCode, 200);
        Joi.assert(res.result, schema);
        t.equal(res.result.title, expectedTitle);
        t.equal(res.result.description, expectedDescription);
        t.equal(res.result.contact, expectedContact);
    });
};


main();
