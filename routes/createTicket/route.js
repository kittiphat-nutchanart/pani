const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');
const assert = require('assert').strict;

const { Ticket } = require('../../models');

module.exports = {
    method: 'POST',
    path: '/api/ticket',
    options: {
        description: 'this endpoint creates a new ticket',
        validate: {
            payload: Joi.object({
                title: Joi.string(),
                description: Joi.string(),
                contact: Joi.string()
            }).prefs({ presence: 'required' })
        }
    },
    handler: async (request, h) => {
        const ticket = await Ticket.query().insertAndFetch(request.payload);

        return ticket;
    }
};
