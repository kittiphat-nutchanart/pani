const tap = require('tap');
const Joi = require('@hapi/joi');
const Knex = require('knex');
const objection = require('objection');

const schema = require('../schema.js');
const serverPromise = require('../../../server');
const knexConfigs = require('../../../knexfile.js');
const {
    Ticket
} = require('../../../models');


async function main() {
    // init knex and tell objection.js to use it
    const knex = Knex(knexConfigs.testing);
    objection.Model.knex(knex);
    const server = await serverPromise;

    tap.test('a list of tickets can be retrieved', async (t) => {
        // given
        // knex setup
        await knex.initialize();
        await knex.migrate.latest();
        t.teardown(async () => {
            await knex.destroy();
        });

        // when
        const creationTime = new Date().toISOString();
        await Ticket.query().insert({
            title: 'title',
            description: 'desc',
            contact: 'contact',
            created_at: creationTime
        });


        const to = new Date().toISOString();
        const res = await server.inject({
            method: 'GET',
            url: `/api/tickets?page=1&size=10&sort=new&to=${to}`,
        });


        // then
        t.equal(res.statusCode, 200);
        Joi.assert(res.result, schema);
        t.equal(res.result.tickets.length, 1);
    });
};


main();
