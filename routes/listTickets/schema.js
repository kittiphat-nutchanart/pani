const Joi = require('@hapi/joi');


module.exports = Joi.object({
    tickets: Joi.array().items(
        Joi.object({
            id: Joi.number(),
            title: Joi.string(),
            description: Joi.string(),
            contact: Joi.string(),
            status: Joi.string(),
            created_at: Joi.date(),
            updated_at: Joi.date()
        })
    ),
    pageCount: Joi.number()
}).prefs({ presence: 'required' });
