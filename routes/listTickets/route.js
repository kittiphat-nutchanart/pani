const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');
const assert = require('assert').strict;

const { Ticket } = require('../../models');

module.exports = {
    method: 'GET',
    path: '/api/tickets',
    options: {
        description: 'this endpoint gives a list of tickets according to filter and sort',
        validate: {
            query: Joi.object({
                status: Joi.string().optional(),
                from: Joi.date().optional(),
                to: Joi.date().optional(),
                size: Joi.number().integer().min(1).max(100).default(25).optional(),
                page: Joi.number().integer().default(1).optional(),
                sort: Joi.string().allow('new', 'old').default('new').optional()
            }).prefs({ presence: 'required' })
        }
    },
    handler: async (request, h) => {
        const { status, from, to, size, page, sort } = request.query;

        const baseQuery = Ticket.query()
              .where(function () {
                  if (status) {
                      this.where('status', status);
                  }

                  if (from) {
                      this.whereRaw(`created_at > '${new Date(from).toISOString()}'`);
                  }

                  if (to) {
                      this.whereRaw(`created_at <= '${new Date(to).toISOString()}'`);
                  }
              });

        const orderByStr = sort === 'new' ? 'created_at desc'
              : 'created_at asc';
        const limitNum = page * (page - 1);
        const tickets = await baseQuery
              .orderByRaw(orderByStr)
              .offset(limitNum)
              .limit(size);

        const ticketCount = await baseQuery.resultSize();
        const pageCount = Math.ceil(ticketCount / size);

        return {
            tickets,
            pageCount
        };
    }
};
