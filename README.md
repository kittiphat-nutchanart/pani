# Pani
    a ticketing system made easy with RESTapi

# it can
- add a new ticket 
  - POST /api/ticket
  - see routes/addTicket/test/route.test.js for more detail
- update a ticket's status
  - PATCH /api/ticket/{id}
  - see routes/updateTicket/test/route.test.js for more detail
- list tickets
  - GET /api/tickets
  - see routes/listTickets/test/route.test.js for more detail

# still missing
- authentication & authorization

# set up
    $ git clone https://gitlab.com/kittiphat-nutchanart/pani

    $ cd pani

    $ npm ci

# run locally
    $ cp .env.template .env

    # update .env file accordingly and make sure that the database is created
    $ vim .env

    $ env $(grep -v '^#' .env | xargs) npx knex migrate:latest --env default

    $ npm run start
    

# run tests
    $ npm run test