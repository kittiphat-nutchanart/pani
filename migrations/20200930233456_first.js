
exports.up = function(knex) {
    return knex.schema
        .createTable('tickets', function(table) {
            table.increments('id');
            table.string('title').notNullable();
            table.string('description').notNullable();
            table.string('contact').notNullable();
            table.enu(
                'status',
                ['pending', 'accepted', 'resolved', 'rejected'],
                { useNative: true, enumName: 'ticket_status' }
            ).notNullable().defaultTo('pending');
            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now());
        });
};

exports.down = function(knex) {
};
